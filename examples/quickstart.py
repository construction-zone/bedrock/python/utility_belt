"""
This example script imports the utility_belt package and
prints out the version.
"""

import utility_belt


def main():
    print(f"utility_belt version: {utility_belt.__version__}")


if __name__ == "__main__":
    main()
