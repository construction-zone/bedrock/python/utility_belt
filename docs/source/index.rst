utility_belt
############

A description of the package.

.. toctree::
   :maxdepth: 2
   :numbered:
   :hidden:

   user/index
   api/index
   dev/index


Quick Start
===========

utility_belt is available on PyPI and can be installed with `pip <https://pip.pypa.io>`_.

.. code-block:: console

    $ pip install utility_belt

After installing utility_belt you can use it like any other Python module.

Here is a simple example:

.. code-block:: python

    import utility_belt
    # Fill this section in with the common use-case.
