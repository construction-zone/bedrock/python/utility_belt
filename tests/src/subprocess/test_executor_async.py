import asyncio

import pytest

from utility_belt import SubprocessExecutorAsync, subprocess

SUCCESS = {
    "returncode": 0,
    "stdout": "Hello World!",
    "stderr": b"",
}
FAILURE = {
    "returncode": 127,
    "stdout": b"",
    "stderr": "not found",
}


async def async_example():
    SE = await SubprocessExecutorAsync().execute(["echo", "Hello World!"])
    assert SE.get("stdout") == SUCCESS.get("stdout")


def test_call_async_example():
    loop = asyncio.get_event_loop()
    tasks = [asyncio.ensure_future(async_example())]
    loop.run_until_complete(asyncio.gather(*tasks))
    loop.close()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "command, expected",
    [
        (["echo", "Hello World!"], SUCCESS),
        (["echo Hello World!"], FAILURE),
        ("echo Hello World!", TypeError(subprocess.MUST_BE_A_LIST)),
    ],
)
async def test_execute(command, expected):
    try:
        SE = await SubprocessExecutorAsync().execute(command)
        assert SE.get("returncode") == expected.get("returncode")
        assert SE.get("stdout") == expected.get("stdout")
        assert expected.get("stderr") in SE.get("stderr")
    except TypeError as e:
        assert type(e) is type(expected) and e.args == expected.args
    except FileNotFoundError as e:
        assert type(e) is type(expected)
