import pytest

from utility_belt import SubprocessExecutor, subprocess
from utility_belt.subprocess import MUST_BE_A_LIST

SUCCESS = {
    "returncode": 0,
    "stdout": "Hello World!",
    "stderr": b"",
}
FAILURE = {
    "returncode": 127,
    "stdout": b"",
    "stderr": "not found",
}


@pytest.mark.parametrize(
    "command, shell, expected",
    [
        (["echo", SUCCESS.get("stdout")], True, SUCCESS),
        (["echo", SUCCESS.get("stdout")], False, FileNotFoundError()),
        ([f"echo {SUCCESS.get('stdout')}"], True, FAILURE),
        ([f"echo {SUCCESS.get('stdout')}"], False, FileNotFoundError()),
        (f"echo {SUCCESS.get('stdout')}", True, TypeError(MUST_BE_A_LIST)),
        (f"echo {SUCCESS.get('stdout')}", False, TypeError(MUST_BE_A_LIST)),
    ],
)
def test_execute(command, shell, expected):
    try:
        SE = SubprocessExecutor(shell=shell).execute(command)
        assert SE.get("returncode") == expected.get("returncode")
        assert SE.get("stdout") == expected.get("stdout")
        assert expected.get("stderr") in SE.get("stderr")
    except TypeError as e:
        assert type(e) is type(expected) and e.args == expected.args
    except FileNotFoundError as e:
        assert type(e) is type(expected)


FAILURE_INJECTION = {
    "returncode": 0,
    "stdout": "> /dev/null ; echo Hello World!",
    "stderr": b"",
}


@pytest.mark.parametrize(
    "command, allow_injection, expected",
    [
        (
            ["echo", "> /dev/null", ";", "echo", SUCCESS.get("stdout")],
            [";", "> /dev/null"],
            SUCCESS,
        ),
        (
            ["echo", "> /dev/null", ";", "echo", SUCCESS.get("stdout")],
            [],
            FAILURE_INJECTION,
        ),
    ],
)
def test_allow_injection(command, allow_injection, expected):
    SExec = SubprocessExecutor(shell=True, allow_injection=allow_injection)
    SE = SExec.execute(command)
    assert SE.get("returncode") == expected.get("returncode")
    assert SE.get("stdout") == expected.get("stdout")
    assert SE.get("stderr") == expected.get("stderr")
