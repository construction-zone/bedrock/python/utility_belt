import logging

from utility_belt import logger_init


def test_init():
    logger_init()
    logging.debug("this is a debug message")
    logging.info("have some good info")
    logging.warning("you have been warned")
    logging.error("welp, an error has occurred")
