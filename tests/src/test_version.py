import utility_belt


def test___init__():
    """check utility_belt exposes a version attribute"""
    assert hasattr(utility_belt, "__version__")
    assert isinstance(utility_belt.__version__, str)
