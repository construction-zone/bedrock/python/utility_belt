def execute_wrapper(**kwargs):
    """
    A warpper for the execute method with SubprocessExecutor the class.

    Used by the SubprocessExecutor class to handle executing commands in a
    consistent way.

    Args:
        is_async (bool): Is the execute method synchronous or asynchronous.
        Defaults to False.
    """

    def func_wrapper(func):
        def wrapper(self, command, stdout_pipe=None, stderr_pipe=None):
            """For synchronous execution"""
            result = func(
                self,
                self._before(command),
                stdout_pipe,
                stderr_pipe,
            )
            return self._after(*result)

        async def async_wrapper(
            self,
            command,
            stdout_pipe=None,
            stderr_pipe=None,
        ):
            """For asynchronous execution"""
            result = await func(
                self,
                self._before(command),
                stdout_pipe,
                stderr_pipe,
            )
            return self._after(*result)

        if kwargs.get("is_async", False):
            return async_wrapper
        return wrapper

    return func_wrapper
