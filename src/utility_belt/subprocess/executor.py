import shlex
from subprocess import PIPE, Popen  # noqa: S404
from typing import Any, Dict, TextIO, Tuple, Union

from . import MUST_BE_A_LIST
from .abc import SubprocessExecutorABC
from .wrapper import execute_wrapper


class SubprocessExecutor(SubprocessExecutorABC):
    """A simple wrapper for subprocesses."""

    def __init__(
        self,
        stdout: Union[int, TextIO] = PIPE,
        stderr: Union[int, TextIO] = PIPE,
        timeout: int = 28800,
        shell: bool = False,
        allow_injection: list = [],
    ):
        """
        For synchronous subprocess execution.

        This module allows you to spawn processes, connect to their
        input/output/error pipes, and obtain their return codes.

        Args:
            stdout (TextIO, optional): An output pipe. Defaults to
                subprocess.PIPE.
            stderr (TextIO, optional): An input pipe. Defaults to
                subprocess.PIPE.
            timeout (int, optional): The maximum time a command can execute in
                seconds. Defaults to 28800.
            shell (bool, optional): If shell is True, the specified command
                will be executed through the shell. Defaults to False.
            allow_injection (bool, optional): [use with caution] A list of
                string, which are allowed to pass through shlex.join. Allowed
                elements must match items in the commad array exactly. Of
                note, this creates the possibility of injection
                vulnerabilities. (i.e [';'] -> ; and ['|'] -> |)
        """
        self.stdout_pipe = stdout
        self.stderr_pipe = stderr
        self.timeout = timeout
        self.shell = shell
        self.allow_injection = allow_injection

    def _before(self, command: list) -> str:
        """
        Validates the commands is a list and returns a sanitized string.

        By Forcing a command to be passed in as a list the risk of command
        line injection is some what mitigated.

        Args:
            command (list): A command in the from of a list.

        Raises:
            TypeError: Command must be of type list.

        Returns:
            str: Shlex joined version of the command.
        """
        if not isinstance(command, list):
            raise TypeError(MUST_BE_A_LIST)

        command_string = shlex.join(command)
        for substr in self.allow_injection:
            command_string = command_string.replace(f"'{substr}'", f"{substr}")
        return command_string

    @execute_wrapper()
    def execute(
        self,
        command: list,
        stdout_pipe: Union[int, TextIO] = None,
        stderr_pipe: Union[int, TextIO] = None,
    ) -> Tuple[Popen, bytes, bytes]:
        """
        Executes a command via subprocess.Popen.

        Runs a subprocess command.

        Args:
            command (list): A command in the from of a list.
            stdout (TextIO, optional): A output pipe. Defaults to
                asyncio.subprocess.PIPE.
            stderr (TextIO, optional): A input pipe. Defaults to
                asyncio.subprocess.PIPE.

        Returns:
            dict: pid, returncode, stdout and stderr

        Raises:
            TypeError: command must be of type list
        """
        process = Popen(  # noqa: DUO116 (mitigation via shlex.join)
            command,
            stdout=stdout_pipe if stdout_pipe else self.stdout_pipe,
            stderr=stderr_pipe if stderr_pipe else self.stderr_pipe,
            shell=self.shell,  # noqa: S602
        )
        output = process.communicate(timeout=self.timeout)
        return process, *output

    def _after(
        self,
        process: Popen,
        stdout: bytes,
        stderr: bytes,
    ) -> Dict[Any, Any]:
        """
        Creates a summary dictionary of results about a command run.

        Args:
            pid (int): the process id
            returncode (str): Exit status of the child process. Typically, an
                exit status of 0 indicates that it ran successfully.
            stdout (str): utf-8 encoded string of the stdout for the
                subprocess run.
            stderr (str): utf-8 encoded string of the stderr for the
                subprocess run.

        Returns:
            dict: Containing pid, returncode, stdout and stderr
        """
        return {
            "pid": process.pid,
            "returncode": process.returncode,
            "stdout": stdout.decode("utf-8").rstrip() if stdout else stdout,
            "stderr": stderr.decode("utf-8").rstrip() if stderr else stderr,
        }
