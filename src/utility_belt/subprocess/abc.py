from abc import ABC, abstractmethod
from subprocess import Popen  # noqa: S404
from typing import Any, Dict, List, TextIO, Tuple, Union

from .wrapper import execute_wrapper


class SubprocessExecutorABC(ABC):
    """A simple abstract base class for subprocesses."""

    @classmethod
    def __init__(
        cls,
        stdout: Union[int, TextIO],
        stderr: Union[int, TextIO],
        timeout: int,
        shell: bool,
        allow_injection: List[str],
    ):
        """
        For asynchronous subprocess execution.

        This module allows you to spawn processes, connect to their
        input/output/error pipes, and obtain their return codes.

        Args:
            stdout (TextIO, optional): An output pipe. Defaults to
                asyncio.subprocess.PIPE.
            stderr (TextIO, optional): An input pipe. Defaults to
                asyncio.subprocess.PIPE.
            timeout (int, optional): The maximum time a command can execute in
                seconds.
            allow_injection (bool, optional): [use with caution] A list of
                string, which are allowed to pass through shlex.join. Allowed
                elements must match items in the commad array exactly. Of
                note, this creates the possibility of injection
                vulnerabilities. (i.e [';'] -> ; and ['|'] -> |)

        Raises:
            TypeError: command must be of type list
        """
        pass

    @abstractmethod
    def _before(self, command: list) -> str:
        """
        Validates the commands is a list and returns a sanitized string.

        Args:
            command (list): A command in the from of a list.

        Raises:
            TypeError: Command must be of type list.

        Returns:
            str: The command string.
        """
        pass

    @classmethod
    @abstractmethod
    @execute_wrapper()
    def execute(
        cls,
        command: list,
        stdout_pipe: Union[int, TextIO] = None,
        stderr_pipe: Union[int, TextIO] = None,
    ) -> Tuple[Popen, bytes, bytes]:
        """
        Executes a command via a something like subprocess.Popen.

        Runs a subprocess command.

        Args:
            command (list): A command in the from of a list.
            stdout (TextIO, optional): A output pipe. Defaults to
                asyncio.subprocess.PIPE.
            stderr (TextIO, optional): A input pipe. Defaults to
                asyncio.subprocess.PIPE.

        Returns:
            dict: pid, returncode, stdout and stderr

        Raises:
            TypeError: command must be of type list
        """
        pass

    @abstractmethod
    def _after(
        self,
        process: Popen,
        stdout: bytes,
        stderr: bytes,
    ) -> Dict[Any, Any]:
        """
        Creates a summary dictionary of results about a command run.

        Args:
            pid (int): the process id
            returncode (str): Exit status of the child process. Typically, an
                exit status of 0 indicates that it ran successfully.
            stdout (str): utf-8 encoded string of the stdout for the
                subprocess run.
            stderr (str): utf-8 encoded string of the stderr for the
                subprocess run.

        Returns:
            dict: pid, returncode, stdout and stderr
        """
        pass
