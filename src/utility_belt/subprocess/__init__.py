"""Used to wrap subprocess and asyncio.subprocess in a consistent manor."""

MUST_BE_A_LIST = "subprocesses command must be of type list."
