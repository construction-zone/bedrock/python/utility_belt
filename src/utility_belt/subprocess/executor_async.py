from asyncio import create_subprocess_shell, wait_for
from asyncio.subprocess import PIPE, Process
from typing import TextIO, Tuple, Union

from .executor import SubprocessExecutor
from .wrapper import execute_wrapper


class SubprocessExecutorAsync(SubprocessExecutor):
    """A simple wrapper for running asyncio subprocess."""

    def __init__(
        self,
        stdout: Union[int, TextIO] = PIPE,
        stderr: Union[int, TextIO] = PIPE,
        timeout: int = 28800,
        allow_injection: list = [],
    ):
        """
        For asynchronous subprocess execution.

        This module allows you to spawn processes, connect to their
        input/output/error pipes, and obtain their return codes.

        Args:
            stdout (TextIO, optional): A output pipe. Defaults to
                asyncio.subprocess.PIPE.
            stderr (TextIO, optional): A input pipe. Defaults to
                asyncio.subprocess.PIPE.
            timeout (int, optional): The maximum time a command can execute in
                seconds. Defaults to 28800.
            allow_injection (bool, optional): [use with caution] A list of
                string, which are allowed to pass through shlex.join. Allowed
                elements must match items in the commad array exactly. Of
                note, this creates the possibility of injection
                vulnerabilities. (i.e [';'] -> ; and ['|'] -> |)

        Raises:
            TypeError: command must be of type list
        """
        super().__init__(
            stdout,
            stderr,
            timeout,
            shell=False,
            allow_injection=allow_injection,
        )

    @execute_wrapper(is_async=True)
    async def execute(
        self,
        command: Union[str, bytes],
        stdout_pipe: Union[int, TextIO] = None,
        stderr_pipe: Union[int, TextIO] = None,
    ) -> Tuple[Process, bytes, bytes]:
        """
        Runs a command via asyncio.create_subprocess_shell.

        Runs a subprocess command.

        Args:
            command (list): For security, command is expected to be of type
            list.
            stdout (TextIO, optional): A output pipe. Defaults to
                asyncio.subprocess.PIPE.
            stderr (TextIO, optional): A input pipe. Defaults to
                asyncio.subprocess.PIPE.
        Returns:
            dict: pid, returncode, stdout and stderr

        Raises:
            TypeError: command must be of type list
        """
        process = await create_subprocess_shell(
            command,
            stdout=stdout_pipe if stdout_pipe else self.stdout_pipe,
            stderr=stderr_pipe if stderr_pipe else self.stderr_pipe,
        )

        output = await wait_for(
            process.communicate(),
            self.timeout,
        )

        return process, *output
