"""A utility bundle."""
import importlib

from .logger.init import init as logger_init
from .subprocess.executor import SubprocessExecutor  # noqa: S404
from .subprocess.executor_async import SubprocessExecutorAsync  # noqa: S404

__version__ = importlib.metadata.version("utility_belt")

__all__ = [
    "logger_init",
    "SubprocessExecutor",
    "SubprocessExecutorAsync",
]
