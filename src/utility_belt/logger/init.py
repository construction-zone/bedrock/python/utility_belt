import logging
from datetime import datetime, timezone


class BasicFormatter(logging.Formatter):
    """A very basic log formatter."""

    def format(self, record):  # noqa: A003
        """A very basic log formater which prepends utc timestamps."""
        now = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        return f"{now}  {logging.Formatter.format(self, record)}"


def init():
    """Initializes a stream_handler and sets log level"""
    format_template = "%(levelno)s %(filename)s:%(lineno)d - %(message)s"
    stream_handler = logging.StreamHandler()

    stream_handler.setFormatter(BasicFormatter(format_template))

    logging.root.level = logging.DEBUG
    logging.root.addHandler(stream_handler)
