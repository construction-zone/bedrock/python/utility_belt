"""A standardize logger."""

import importlib.metadata

__version__ = importlib.metadata.version("utility_belt")
